package method

func Min(a float64, b float64, c float64) float64 {
	mass := []float64{a, b, c}
	min := mass[0]
	for i := 1; i < len(mass); i++ {
		if min > mass[i] {
			min = mass[i]
		}
	}
	return min
}
func Avg(a float64, b float64, c float64) float64 {
	return (a + b + c) / 3
}

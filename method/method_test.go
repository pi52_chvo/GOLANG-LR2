package method

import "testing"

func TestMin(t *testing.T) {
	x := Min(1, 2, -3)
	res := float64(-3)
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}
func TestAvg(t *testing.T) {
	x := Avg(6, 3, -3)
	res := float64(2)
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}
